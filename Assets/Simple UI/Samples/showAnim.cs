﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showAnim : MonoBehaviour
{
	private Animator anim;
	
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator>();
	}

	public void theHit()
	{
		anim.SetTrigger("hit");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
