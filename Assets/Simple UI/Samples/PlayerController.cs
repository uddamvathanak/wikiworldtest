﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IBM.Watson.DeveloperCloud.Services.TextToSpeech.v1;
using IBM.Watson.DeveloperCloud.DataTypes;
using IBM.Watson.DeveloperCloud.Utilities;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Connection;
using System.IO;
using FullSerializer;
using IBM.Watson.DeveloperCloud.Services.Discovery.v1;
using UnityEngine.Playables;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour {

	//You need to provide some string of text to say.
	private string outputText = "Hi there";
	private TextToSpeech _textToSpeech;
	public Button PButton;
	
	private static Animator anim;

	[SerializeField]
	private fsSerializer _serializer = new fsSerializer();

	void Start()
	{
		
		
		Credentials credentials2 = new Credentials("ccf0f2ce-6fdc-434d-8b4d-d943730aa5cd", "kdjXTvGAmQi6", "https://stream.watsonplatform.net/text-to-speech/api");
		_textToSpeech = new TextToSpeech(credentials2);

		//Make sure you give Watson a voice type
		_textToSpeech.Voice = VoiceType.en_US_Michael;

		//Create a button for onclick listener
		
		Button btn1 = PButton.GetComponent<Button>();

		//Calls the TaskOnClick/TaskWithParameters method when you click the Button
		btn1.onClick.AddListener(playsound);
		
		//get animator object
		anim = GetComponent<Animator>();
	}

	//build the audio player with successful response
	public static void OnSynthesize(AudioClip clip, Dictionary<string, object> customData)
	{
		PlayClip(clip);
	}

	//you need to create an AudioObject so Unity can play the audio file sent back by Watson
	public static void PlayClip(AudioClip clip)
	{
		if (Application.isPlaying && clip != null)
		{
			GameObject audioObject = new GameObject("AudioObject");
			AudioSource source = audioObject.AddComponent<AudioSource>();
			source.spatialBlend = 0.0f;
			source.loop = false; //that sounds annoying
			source.clip = clip; //the actual audio bits
			source.Play();

			Destroy(source, clip.length);
			Destroy(audioObject, clip.length);
			Destroy(anim, clip.length);
			
		}

	}

	private void OnFail(RESTConnector.Error error, Dictionary<string, object> customData)
	{
		Log.Error("ExampleTextToSpeech.OnFail()", "Error received: {0}", error.ToString());
	}

	private void playsound()
	{
		//send the outputText, and in the simpliest example set streaming to false as this will be a short sentence
		if(!_textToSpeech.ToSpeech(OnSynthesize, OnFail, outputText, false))
			Log.Debug("ExampleTextToSpeech.ToSpeech()", "Failed to synthesize!");
	}
	
	public void theHit()
	{
		anim.SetTrigger("hit");
	}

	private void Update()
	{

	}

	
}