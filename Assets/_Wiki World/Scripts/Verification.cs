﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Verification : MonoBehaviour {

    public Text emailLabel;

	// Use this for initialization
	void Start () {
        emailLabel.text = FirebaseController.userEmail;
	}

    public void checkVerification()
    {
        Debug.Log("Verification button is clicked");
        if (!Loader.isPaused)
        {
            Debug.Log("Checking Verification...");
            Debug.Log(FirebaseController.userEmail + FirebaseController.userPassword);
            FirebaseController.SigninWithEmailCredentialAsync(FirebaseController.userEmail, FirebaseController.userPassword);
        }

    }

    public void ResendNotification()
    {
        if (!Loader.isPaused)
        {
            FirebaseController.SendVerificationEmail();
        }
        
    }

    void OnApplicationFocus(bool hasFocus)
    {
        // if true, application has resumed
        if (hasFocus)
        {
            Debug.Log("hello " + FirebaseController.userEmail + " " + FirebaseController.userPassword);
            checkVerification();
        }
    }


    // Update is called once per frame
    void Update () {
		
	}
}
