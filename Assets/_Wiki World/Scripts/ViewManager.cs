﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class ViewManager : MonoBehaviour {

    public static void ProceedNextScene(int aNumber)
    {
        switch (aNumber)
        {
            case 4:
                SceneManager.LoadSceneAsync("Menu");
                break;
            case 3:
                SceneManager.LoadSceneAsync("Verification");
                break;
            case 2:
                SceneManager.LoadSceneAsync("Volunteer");
                break;
            case 1:
                Debug.Log("Scene Not Generated Yet!");
                break;
            default:
                SceneManager.LoadSceneAsync("Registration");
                break;
        }
        
        //SceneManager.LoadSceneAsync("Menu");
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
