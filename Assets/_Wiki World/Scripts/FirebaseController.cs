﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Storage;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

public static class Validator
{

    static Regex ValidEmailRegex = CreateValidEmailRegex();

    /// <summary>
    /// Taken from http://haacked.com/archive/2007/08/21/i-knew-how-to-validate-an-email-address-until-i.aspx
    /// </summary>
    /// <returns></returns>
    private static Regex CreateValidEmailRegex()
    {
        string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
    }

    internal static bool EmailIsValid(string emailAddress)
    {
        bool isValid = ValidEmailRegex.IsMatch(emailAddress);

        return isValid;
    }
}

public class FirebaseController : MonoBehaviour
{
    // Firebase Authentication
    public static Firebase.Auth.FirebaseAuth auth;
    public static Dictionary<string, Firebase.Auth.FirebaseUser> userByAuth;
    public static Firebase.Auth.FirebaseUser user;

    // Firebase Database
    public static DatabaseReference reference;
    public static FirebaseApp app;

    // Firebase Storage
    public static StorageReference storage_ref;
    public static FirebaseStorage storage;
    public static StorageReference img_ref;
    protected string MyStorageBucket = "gs://wiki-world-2bb42.appspot.com/";

    // Firebase Variables
    public static string imageList = "uploads";
    public static string dbTitle = "users";
    public static string dbChild = "email";
    public static string imageTemplate = "images";
    public static string imageExtension = "jpg";

    public static string userEmail = "test@gmail.com";
    public static string userPassword = "testing123";
    public static string displayName = "John Doe";
    public static string userID = "zY6g8nDSRT";

    public static bool isEmailValid(string email)
    {
        bool isValid = Validator.EmailIsValid(email);
        if (!isValid)
        {
            ToastManager.MyShowToastMethod("Malformed Email Information!");
        }
        return isValid;
    }

    public static bool isPasswordValid(string password)
    {
        if (password.Length < 8)
        {
            ToastManager.MyShowToastMethod("Password must be at least 8 characters long");
            return false;
        }
        return true;
    }

    public static void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        Firebase.Auth.FirebaseAuth senderAuth = sender as Firebase.Auth.FirebaseAuth;
        user = null;
        if (senderAuth != null) userByAuth.TryGetValue(senderAuth.App.Name, out user);
        if (senderAuth == auth && senderAuth.CurrentUser != user)
        {
            Debug.Log("Gotcha1");
            bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = senderAuth.CurrentUser;
            userByAuth[senderAuth.App.Name] = user;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                RemoveSpinnerCall();
                //DisplayDetailedUserInfo(user, 1);
            }
        }
    }

    public static void resetPW(string requestedEmail)
    {
        string emailAddress = requestedEmail;
        if (!isEmailValid(requestedEmail))
        {
            Debug.Log("Invalid Email");
            return;
        }
        Debug.Log("Valid Email");
        InternalSpinnerCall();
        auth.SendPasswordResetEmailAsync(emailAddress).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                RemoveSpinnerCall();
                return;
            }
            if (task.IsFaulted)
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                RemoveSpinnerCall();
                return;
            }

            Debug.Log("Password reset email sent successfully.");
            ToastManager.MyShowToastMethod("Email Reset Sent!");
            RemoveSpinnerCall();
        });
    }

    public static void changePW(string newPass)
    {
        string newPassword = newPass;
        if (string.IsNullOrEmpty(newPass) || newPass.Length < 8)
        {
            //ToastManager.MyShowToastMethod("Password must contain at least 8 character!");
            ViewManager.ProceedNextScene(4);
            return;
        }

        if (user != null)
        {
            InternalSpinnerCall();
            user.UpdatePasswordAsync(newPassword).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("UpdatePasswordAsync was canceled.");
                    RemoveSpinnerCall();
                    ViewManager.ProceedNextScene(4);
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("UpdatePasswordAsync encountered an error: " + task.Exception);
                    RemoveSpinnerCall();
                    ViewManager.ProceedNextScene(4);
                    return;
                }
                RemoveSpinnerCall();
                ToastManager.MyShowToastMethod("Password Updated...");
                Debug.Log("Password updated successfully.");
                PlayerPrefs.SetString("Password", newPass);
                ViewManager.ProceedNextScene(4);
            });
        }
    }

    public static void CreateUserWithEmailAsync(string email, string password)
    {
        if (!isEmailValid(email))
        {
            return;
        }

        if (!isPasswordValid(password))
        {
            return;
        }

        InternalSpinnerCall();
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                RemoveSpinnerCall();
                return;
            }
            if (task.IsFaulted)
            {
                // Account already exist
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                RemoveSpinnerCall();
                ToastManager.MyShowToastMethod("User already exist");
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
            /*
            // Send Email Verification Messsage
            newUser.SendEmailVerificationAsync().ContinueWith(t => {
                Debug.Log("SendEmailVerificationAsync Success");
            });
            */
            userEmail = newUser.Email;
            userID = newUser.UserId;
            userPassword = password;
            PlayerPrefs.SetString("Email", email);
            PlayerPrefs.SetString("Password", password);
            PlayerPrefs.Save();
            SendVerificationEmail();
            ViewManager.ProceedNextScene(3);
        });
    }

    public static void RegainAuth()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.SignOut();
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(auth, null);
    }

    public static void SigninWithEmailCredentialAsync(string email, string password)
    {
        if (!isEmailValid(email))
        {
            return;
        }

        if (!isPasswordValid(password))
        {
            return;
        }
        InternalSpinnerCall();
        //email = "richmondgoh@gmail.com";
        //password = "12345678";
        auth.SignOut();

        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                RemoveSpinnerCall();
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                RemoveSpinnerCall();
                ToastManager.MyShowToastMethod("Invalid username or password");
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
            userEmail = email;
            userPassword = password;
            userID = newUser.UserId;

            // Since successfully, proceed to get value and write to preference file
            Debug.Log(newUser.IsEmailVerified); //false should sign out and spawn a dialog
            Debug.LogFormat("user name is: {0}", newUser.DisplayName);

            // Write to file
            PlayerPrefs.SetString("Email", email);
            PlayerPrefs.SetString("Password", password);
            PlayerPrefs.Save();
            RemoveSpinnerCall();
            if (user.IsEmailVerified)
            {
                ViewManager.ProceedNextScene(4);
            }
            else
            {
                ViewManager.ProceedNextScene(3);
            }

        });
    }

    public static void UpdateUserProfileAsync(string newDisplayName, string newPass)
    {
        Debug.Log("Updating user profile");
        if (string.IsNullOrEmpty(newDisplayName))
        {
            changePW(newPass);
            return;
        }
        //Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            InternalSpinnerCall();
            Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile
            {
                DisplayName = newDisplayName,
                //PhotoUrl = new System.Uri("https://example.com/jane-q-user/profile.jpg"),
            };
            user.UpdateUserProfileAsync(profile).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    RemoveSpinnerCall();
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                    RemoveSpinnerCall();
                    return;
                }

                Debug.Log("User profile updated successfully.");
                displayName = newDisplayName;
                RemoveSpinnerCall();
                changePW(newPass);
            });
        }
    }

    public static void SendVerificationEmail()
    {
        if (user != null)
        {
            InternalSpinnerCall();
            user.SendEmailVerificationAsync().ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("SendEmailVerificationAsync was canceled.");
                    RemoveSpinnerCall();
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SendEmailVerificationAsync encountered an error: " + task.Exception);
                    RemoveSpinnerCall();
                    ToastManager.MyShowToastMethod("Error Trying to send email");
                    return;
                }
                Debug.Log("Email sent successfully.");
                RemoveSpinnerCall();
            });
        }
    }

    public static void FirebaseLogout()
    {
        PlayerPrefs.SetInt("RememberMe", 0);
        PlayerPrefs.Save();
        ViewManager.ProceedNextScene(5);
    }

    public static void InternalSpinnerCall()
    {
        var spinnerPlaceholder = GameObject.FindGameObjectWithTag("LoaderBuffer");
        var bobo = spinnerPlaceholder.GetComponent<Loader>();
        bobo.onPause();
    }

    public static void RemoveSpinnerCall()
    {
        var spinnerPlaceholder = GameObject.FindGameObjectWithTag("LoaderBuffer");
        var bobo = spinnerPlaceholder.GetComponent<Loader>();
        bobo.onResume();
    }

    protected virtual void InitializeFirebase()
    {
        InternalSpinnerCall();
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.SignOut();

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);

        // Accessing this script means that user email & id is available
        // LoginHandler.userEmail;
        // LoginHandler.userID;
        userByAuth = new Dictionary<string, Firebase.Auth.FirebaseUser>();

        app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl("https://wiki-world-2bb42.firebaseio.com/");
        reference = FirebaseDatabase.DefaultInstance.RootReference;

        storage = FirebaseStorage.DefaultInstance;
        storage_ref = storage.GetReferenceFromUrl(MyStorageBucket);

        // Should proceed to check user preference and see value
        int myInt = PlayerPrefs.GetInt("RememberMe");
        string email = PlayerPrefs.GetString("Email");
        string password = PlayerPrefs.GetString("Password");

        // Default is 0
        if (myInt == 0)
        {
            RemoveSpinnerCall();
            // Remember me is deactivated
            // Do nothing
        }
        else
        {
            SigninWithEmailCredentialAsync(email, password);
            // Rememberme is activated
        }

    }


    // Use this for initialization
    void Awake()
    {
        InitializeFirebase();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
        //auth.StateChanged -= AuthStateChanged;
        //auth = null;
    }


}
