﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using Firebase;
using Firebase.Storage;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Firebase.Unity.Editor;
using Firebase.Database;
using System.Linq;
using System.Text.RegularExpressions;

public class CameraScript : MonoBehaviour
{
    public TextAsset nameList;

    private static ILogger logger = Debug.unityLogger;
    //public Image rawImg;
    public RawImage rawImg;
    //static WebCamTexture webcamTexture;
    static WebCamTexture backCam;
    public InputField trailerUrl;
    public InputField synopsisText;
    protected string MyStorageBucket = "gs://wiki-world-2bb42.appspot.com/";
    //protected static string UriFileScheme = Uri.UriSchemeFile + "://";
    protected string fileMetadataChangeString = "";
    protected string fileContents;
    protected Texture2D snap;
    protected byte[] bytes;
    protected byte[] rotatedBytes;

    public static List<string> exclusiveList;
    public static List<string> sequelCommonList;

    // Button for Cloud Vision API
    public GameObject suggestBtn;
    private int suggestCount; // 1 per snap
    private int suggestLimit = 1;

    // Cloud Storage location to download from / upload to.
    protected string storageLocation;

    // Hold a reference to the FirebaseStorage object so that we're not reinitializing the API on
    // each transfer.
    protected StorageReference storage_ref;
    protected StorageReference img_ref;
    DatabaseReference reference;

    // Whether an operation is in progress.
    protected bool operationInProgress;

    // Previously completed task.
    protected Task previousTask;

    // Cancellation token source for the current operation.
    protected CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

    private bool firebaseInitalized = false;

    private string randomID;

    //StorageReference storage_ref;
    private List<string> myCollection = new List<string>();

    // Google Vision Initalization
    // https://github.com/comoc/UnityCloudVision/blob/master/Assets/WebCamTextureToCloudVision.cs

    public string url = "https://vision.googleapis.com/v1/images:annotate?key=";
    public string apiKey = "AIzaSyAzM3WBgi0tFfJ2lbVnLja5sng_V3u_5RQ";
    public FeatureType featureType = FeatureType.TEXT_DETECTION;
    public int maxResults = 10;
    Dictionary<string, string> headers;

    [System.Serializable]
    public class AnnotateImageRequests
    {
        public List<AnnotateImageRequest> requests;
    }

    [System.Serializable]
    public class AnnotateImageRequest
    {
        public Image image;
        public List<Feature> features;
    }

    [System.Serializable]
    public class Image
    {
        public string content;
    }

    [System.Serializable]
    public class Feature
    {
        public string type;
        public int maxResults;
    }

    [System.Serializable]
    public class AnnotateImageResponses
    {
        public List<AnnotateImageResponse> responses;
    }

    [System.Serializable]
    public class AnnotateImageResponse
    {
        public List<EntityAnnotation> labelAnnotations;
        public List<EntityAnnotation> textAnnotations;
    }

    [System.Serializable]
    public class EntityAnnotation
    {
        public string mid;
        public string locale;
        public string description;
        public float score;
        public float confidence;
        public float topicality;
        public List<LocationInfo> locations;
        public List<Property> properties;
    }

    [System.Serializable]
    public class Property
    {
        string name;
        string value;
    }

    public enum FeatureType
    {
        LABEL_DETECTION,
        TEXT_DETECTION,
    }

    protected virtual void InitializeFirebase()
    {
        Debug.Log("Initalizing");
        storage_ref = FirebaseController.storage_ref;
        reference = FirebaseController.reference;

        // Create list of exclusive words
        exclusiveList = new List<string> { "A", "FILM", "DISNEY", "PIXAR", "ENTRY", "AWARDS", "BY", "ACADEMY", "MOTION", "COMING", "THIS", "SOON", "JOHN", "COLIN", "RACHEL", "COLMAN", "ASHLEY", "ARIANE", "LEA", "MICHAEL", "BEN" };
        // Common Words in Sequel
        sequelCommonList = new List<string> { "FIRE", "BACK", "REVENGE", "FIRST", "LAST", "FINAL", "NEXT", "RISE", "RETURN", "DARK", "GAME", "WORLD" };

        myCollection = new List<string>();
        reference.Child(FirebaseController.dbTitle).OrderByChild(FirebaseController.dbChild).EqualTo(FirebaseController.userEmail).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("A Horrible Error Occured");
            }
            else if (task.IsCompleted)
            {
                firebaseInitalized = true;

                // if no existing, it will return nothing for key and value
                DataSnapshot snapshot = task.Result;
                if (snapshot.Value != null)
                {
                    // Do something here if table value don't exist                
                    var KeyFinder = snapshot.Value as Dictionary<string, object>;
                    string mainKey = "";
                    foreach (var item in KeyFinder)
                    {
                        mainKey = item.Key;
                    }


                    var items = snapshot.Child(mainKey).Value as Dictionary<string, object>;
                    List<object> allRecords = items[FirebaseController.imageList] as List<object>;
                    foreach (var record in allRecords)
                    {
                        Debug.Log(record);

                        myCollection.Add(record.ToString());
                    }
                    Debug.Log("Found " + myCollection.Count.ToString() + "Documents");
                }
            }
        });
    }

    // Write upload state to the log.
    protected virtual void DisplayUploadState(UploadState uploadState)
    {
        if (operationInProgress)
        {
            Debug.Log(String.Format("Uploading {0}: {1} out of {2}", uploadState.Reference.Name,
                                   uploadState.BytesTransferred, uploadState.TotalByteCount));
        }
    }

    static Texture2D rotate(Texture2D t)
    {
        Texture2D newTexture = new Texture2D(t.height, t.width, t.format, false);

        for (int i = 0; i < t.width; i++)
        {
            for (int j = 0; j < t.height; j++)
            {
                newTexture.SetPixel(j, i, t.GetPixel(t.width - i, j));
            }
        }
        newTexture.Apply();
        return newTexture;
    }

    public void RetakeBtn()
    {
        if (!backCam.isPlaying)
        {
            rawImg.texture = backCam;
            rawImg.material.mainTexture = backCam;
            backCam.Play();
            suggestBtn.SetActive(false);
        }
    }

    public void TakeSnapshot()
    {

        if (!Loader.isPaused && backCam.isPlaying)
        {
            snap = new Texture2D(backCam.width, backCam.height);
            snap.SetPixels(backCam.GetPixels());
            snap.SetPixels32(backCam.GetPixels32());
            snap.Apply();
            backCam.Stop();
            rawImg.texture = snap;
            rawImg.material.mainTexture = snap;

            // Prepare render for upload
            Texture2D photo = new Texture2D(backCam.width, backCam.height);
            photo = rotate(snap);
            photo.Apply();

            bytes = photo.EncodeToPNG();
            Debug.Log(bytes);
            suggestCount = 0;
            suggestBtn.SetActive(true);
        }
    }

    public void ProvideSuggestion()
    {
        if (suggestCount < suggestLimit)
        {
            FirebaseController.InternalSpinnerCall();
            StartCoroutine("RequestVision");
            suggestCount++;
        }

    }

    private IEnumerator RequestVision()
    {
        Debug.Log("Requesting Vision");
        headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json; charset=UTF-8");

        if (apiKey == null || apiKey == "")
        {
            Debug.LogError("No API key. Please set your API key into the \"Web Cam Texture To Cloud Vision(Script)\" component.");
        }

        string base64 = Convert.ToBase64String(bytes);
        AnnotateImageRequests requests = new AnnotateImageRequests();
        requests.requests = new List<AnnotateImageRequest>();

        AnnotateImageRequest request = new AnnotateImageRequest();
        request.image = new Image();
        //request.image.content = base64;
        request.image.content = base64;
        request.features = new List<Feature>();

        Feature feature = new Feature();
        feature.type = featureType.ToString();
        feature.maxResults = maxResults;

        request.features.Add(feature);
        requests.requests.Add(request);


        string jsonData = JsonUtility.ToJson(requests, false);
        if (jsonData != string.Empty)
        {
            string myurl = url + apiKey;
            byte[] postData = Encoding.Default.GetBytes(jsonData);
            using (WWW www = new WWW(myurl, postData, headers))
            {
                Debug.Log("Came in Here!");
                yield return www;
                if (string.IsNullOrEmpty(www.error))
                {
                    //Debug.Log(www.text.Replace("\n", "").Replace(" ", ""));
                    AnnotateImageResponses responses = JsonUtility.FromJson<AnnotateImageResponses>(www.text);
                    // SendMessage, BroadcastMessage or someting like that.
                    Sample_OnAnnotateImageResponses(responses);
                }
                else
                {
                    Debug.Log("Error: " + www.error);
                }
                FirebaseController.RemoveSpinnerCall();
            }
        }
    }



    void Sample_OnAnnotateImageResponses(AnnotateImageResponses responses)
    {
        //bool responseCaught = false;
        if (responses.responses.Count > 0)
        {
            if (responses.responses[0].textAnnotations != null && responses.responses[0].textAnnotations.Count > 0)
            {
                string powerText = responses.responses[0].textAnnotations[0].description;
                logger.Log("Crazylog", powerText);
                string[] lines = powerText.Split('\n');
                foreach (string line in lines)
                {
                    if (line.Contains(" "))
                    {
                        logger.Log("Potential", line);
                        if (sequelCommonList.Any(line.ToUpper().Contains))
                        {
                            //Potential Hit Count
                            trailerUrl.text = line;
                            break;
                        }

                        if (!exclusiveList.Any(line.ToUpper().Contains))
                        {
                            // Not in Exclusion List (Another Potential)
                            trailerUrl.text = line;
                            break;
                        }
                    }

                }
                ToastManager.MyShowToastMethod("Search Completed!");
            }
            else
            {
                ToastManager.MyShowToastMethod("Search shown nothing!");
            }
        }
    }

    public class Contribution
    {
        public string email;
        public List<string> uploads = new List<string>();

        public Contribution()
        {

        }

        public Contribution(string email, List<string> uploads)
        {
            this.uploads = uploads;

            this.email = email;

        }
    }

    public void UploadObj()
    {
        if (Loader.isPaused)
        {
            return;
        }

        if (RateGame.movieRating == 0)
        {
            Debug.Log("Please rate the game before continuing");
            return;
        }

        FirebaseController.InternalSpinnerCall();
        Debug.Log(firebaseInitalized);
        if (!firebaseInitalized)
        {
            FirebaseController.RemoveSpinnerCall();
            Debug.Log("Firebase have yet to initalized");
            return;
        }
        Debug.Log("Firebase Initalized");


        // Create a reference to the file you want to upload (Email + )
        randomID = Guid.NewGuid().ToString();
        string firebaseUrl = String.Format("{0}/{1}.{2}", FirebaseController.imageTemplate, randomID, FirebaseController.imageExtension);
        img_ref = storage_ref.Child(firebaseUrl);


        var new_metadata = new MetadataChange
        {
            CustomMetadata = new Dictionary<string, string>
            {
                {"Uploader", FirebaseController.userEmail},
                {"Movie Title", trailerUrl.text},
                {"Review", synopsisText.text},
                {"Rating", RateGame.movieRating.ToString()}
            }
        };

        img_ref.PutBytesAsync(bytes, new_metadata, null, CancellationToken.None, null)
          .ContinueWith((Task<StorageMetadata> task) =>
          {
              if (task.IsFaulted || task.IsCanceled)
              {
                  Debug.Log(task.Exception.ToString());
                  FirebaseController.RemoveSpinnerCall();
                  RetakeBtn();
                  // Uh-oh, an error occurred!
              }
              else
              {
                  // Metadata contains file metadata such as size, content-type, and download URL.
                  StorageMetadata metadata = task.Result;
                  Debug.Log("Photo Uploaded...");
                  myCollection.Add(randomID);
                  Contribution contributor = new Contribution(FirebaseController.userEmail, myCollection);
                  Debug.Log("Collection Count" + myCollection.Count.ToString());
                  Debug.Log(contributor.uploads.Count.ToString());
                  string json = JsonUtility.ToJson(contributor);
                  reference.Child(FirebaseController.dbTitle).Child(FirebaseController.userID).SetRawJsonValueAsync(json);
                  Debug.Log("User Data Updated");
                  FirebaseController.RemoveSpinnerCall();
                  trailerUrl.text = "";
                  synopsisText.text = "";
                  RetakeBtn();
                  RateGame.Instance.RateApplication(0);
              }
          });


    }

    public void initateList()
    {
        string fs = nameList.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for (int i = 0; i < fLines.Length - 1; i++)
        {
            string valueLine = fLines[i];
            //Debug.Log(valueLine);
            exclusiveList.Add(valueLine.ToUpper());
        }
    }

    void Start()
    {
        //Debug.Log(nameList.text);
        suggestBtn.SetActive(false);
        Debug.Log("Set False");
        InitializeFirebase();
        initateList();
        backCam = new WebCamTexture(1080, 1080, 30);
        rawImg.texture = backCam;
        rawImg.material.mainTexture = backCam;
        backCam.Play();
        var orient = -backCam.videoRotationAngle;
        rawImg.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        Debug.Log("Camera Ready");


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ViewManager.ProceedNextScene(4);
        }
    }
}
