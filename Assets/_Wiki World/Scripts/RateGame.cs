﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class RateGame : MonoBehaviour
{

    public Button[] starButton;
    //public GameObject mainPanel;
    //public Button acceptButton;
    public static RateGame Instance;

    [HideInInspector]
    public static int movieRating = 0;

    public void RateApplication(int rate)
    {
        for (int i = 0; i < starButton.Length; i++)
        {
            starButton[i].GetComponent<Image>().color = new Color32(0, 0, 0, 255);
            if (i < rate)
            {
                starButton[i].GetComponent<Image>().color = new Color32(255, 255, 225, 255);
            }
            
        }

        movieRating = rate;
    }

    void Awake()
    {
        Instance = this;
    }

}
