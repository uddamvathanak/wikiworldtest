﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;

public class MenuHandler : MonoBehaviour {

    public Text titleText;
    public Text contributionText;

    public InputField newName;
    public InputField passwordField;
    public InputField cfmPasswordField;

    public Canvas overlayCanvas;
    private static bool overlayOn = false;

    public void HandleBtn(int SceneIndex)
    {
        if (!overlayOn)
        {
            ViewManager.ProceedNextScene(SceneIndex);
        } else
        {
            Debug.Log("cant pierce through");
        }  
    }

    protected virtual void InitializeFirebase()
    {
        FirebaseController.InternalSpinnerCall();
        FirebaseApp app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl("https://wiki-world-2bb42.firebaseio.com/");

        var reference = FirebaseController.reference;

        string bokoText;
        
        reference.Child(FirebaseController.dbTitle).OrderByChild(FirebaseController.dbChild).EqualTo(FirebaseController.userEmail).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("A Horrible Error Occured");
                FirebaseController.RemoveSpinnerCall();
            }
            else if (task.IsCompleted)
            {
                Debug.Log("task came here");
                // if no existing, it will return nothing for key and value
                DataSnapshot snapshot = task.Result;

                if (snapshot == null)
                {
                    Debug.Log("SNAPSHOT IS NULL");
                    bokoText = String.Format("Contribution: {0}", 0);
                    FirebaseController.RemoveSpinnerCall();
                    contributionText.text = bokoText;
                    return;
                }
      
                if (snapshot.Value != null)
                {
                    Debug.Log("Snapshot Not Null");
                    // Do something here if table value don't exist                
                    var KeyFinder = snapshot.Value as Dictionary<string, object>;
                    string mainKey = "";
                    foreach (var item in KeyFinder)
                    {
                        mainKey = item.Key;
                    }

                    if (mainKey == "")
                    {
                        Debug.Log("No Key Value");
                        return;
                    }

                    var items = snapshot.Child(mainKey).Value as Dictionary<string, object>;
                    List<object> allRecords = items[FirebaseController.imageList] as List<object>;
                    bokoText = String.Format("Contribution: {0}", allRecords.Count);

                }
                else
                {
                    bokoText = String.Format("Contribution: {0}", 0);
                }
                FirebaseController.RemoveSpinnerCall();
                contributionText.text = bokoText;

            }
        });
       
    }

    public void toggleSettings()
    {
        overlayCanvas.enabled = !overlayCanvas.enabled;
        overlayOn = overlayCanvas.enabled;
        Debug.Log(overlayOn);
    }

    public void UpdateProfile()
    {
        string newDisplayName = newName.text;
        string currentPassword = passwordField.text;
        string newCfmPassword = cfmPasswordField.text;

        toggleSettings();

        if (string.IsNullOrEmpty(newDisplayName) && (string.IsNullOrEmpty(currentPassword)))
        {
            ToastManager.MyShowToastMethod("Nothing to Update");
            Debug.Log("nothing to Update");
            return;
        }

        if (currentPassword != newCfmPassword)
        {
            ToastManager.MyShowToastMethod("Password do not match!");
            Debug.Log("Password no match");
            return;
        }

        Debug.LogFormat("Trying to change display name to {0}", newName.text);
        
        FirebaseController.UpdateUserProfileAsync(newName.text, currentPassword);
        Debug.Log("Done");
    }

    public void SignOut()
    {
        toggleSettings();
        FirebaseController.FirebaseLogout();
    }

    // Use this for initialization
    void Start () {
        overlayOn = false;
        InitializeFirebase();
        string fullStr;
        if (!string.IsNullOrEmpty(FirebaseController.displayName))
        {
            fullStr = String.Format("Welcome {0}", FirebaseController.displayName);
        } else
        {
            fullStr = String.Format("Welcome {0}", FirebaseController.userEmail);
        }

        // Should get username instead

        titleText.text = fullStr;
    }
	
	// Update is called once per frame
	void Update () {
		
	}


}
