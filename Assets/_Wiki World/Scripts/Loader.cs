﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Loader : MonoBehaviour {

    public GameObject panelLoader;
    public Canvas mainCanvas;
    private GameObject germSpawned;
    public static bool isPaused;

    public void onPause()
    {
        Debug.Log("ON PAUSE CALLED");
        isPaused = true;
        if (germSpawned == null)
        {
            Debug.Log("Process Paused");
            SpawnSpinner();
        }
        
    }

    public void onResume()
    {
        Debug.Log("ON RESUME CALLED");
        isPaused = false;
        if (germSpawned != null)
        {
            Debug.Log("Process Resume");
            
            Destroy(germSpawned);
            germSpawned = null;
        }
    }

    public void SpawnSpinner()
    {
        germSpawned = Instantiate(panelLoader) as GameObject;
        germSpawned.transform.SetParent(mainCanvas.transform);
        germSpawned.transform.localScale = new Vector3(1, 1, 1);
        var mySetting = germSpawned.GetComponent<RectTransform>();
        mySetting.offsetMin = new Vector2(0, 0); // new Vector2(left, bottom); 
        mySetting.offsetMax = new Vector2(0, -0); // new Vector2(-right, -top);
    }

	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
	}
}
