// Copyright 2016 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Handler for UI buttons on the scene.  Also performs some
// necessary setup (initializing the firebase app, etc) on
// startup.
public class LoginHandler : MonoBehaviour
{
    public InputField emailText;
    public InputField passwordText;
    public Toggle m_Toggle;
    public static bool rememberMe = false;

    
    

    // When the app starts, check to make sure that we have
    // the required dependencies to use Firebase, and if not,
    // add them if possible.
    public virtual void Start()
    {
    }



    // Exit if escape (or back, on mobile) is pressed.
    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    // Create a user with the email and password.
    public void CreateUserWithEmailAsync()
    {
        if (!Loader.isPaused)
        {
            FirebaseController.CreateUserWithEmailAsync(emailText.text, passwordText.text);
        }
    }
    // This is functionally equivalent to the Signin() function.  However, it
    // illustrates the use of Credentials, which can be aquired from many
    // different sources of authentication.
    public void SigninWithEmailCredentialAsync()
    {
        if (!Loader.isPaused)
        {
            FirebaseController.SigninWithEmailCredentialAsync(emailText.text, passwordText.text);
        }
    }

    //Output the new state of the Toggle into Text
    public void ToggleValueChanged(Toggle change)
    {
        Debug.Log(m_Toggle.isOn);
        rememberMe = m_Toggle.isOn;
        if (m_Toggle.isOn)
        {
            PlayerPrefs.SetInt("RememberMe", 1);
        } else
        {
            PlayerPrefs.SetInt("RememberMe", 0);
        }
        
        //PlayerPrefs.Save();
        Debug.Log("Is now " + rememberMe.ToString());
        //m_Text.text = "New Value : " + m_Toggle.isOn;
        
    }

    public void ResetPW()
    {
        if (!Loader.isPaused)
        {
            Debug.Log("trying to reset password");
            FirebaseController.resetPW(emailText.text);
        }
    }

}
